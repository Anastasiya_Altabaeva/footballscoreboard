package com.example.footballscoreboard;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private Integer goalsGermany = 0;

    private Integer goalsUSA = 0;

    private TextView goals1Team;

    private TextView goals2Team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        goals1Team = findViewById(R.id.goalsFirstTeam);

        goals2Team = findViewById(R.id.goalsSecondTeam);
    }

    public void onClickFirstTeam(View view) {

        goalsGermany++;

        goals1Team.setText(goalsGermany.toString());
    }

    public void onClickSecondTeam(View view) {

        goalsUSA++;

        goals2Team.setText(goalsUSA.toString());
    }

    public void onClickReset(View view) {

        goalsUSA = 0;

        goalsGermany = 0;

        goals2Team.setText(goalsUSA.toString());

        goals1Team.setText(goalsGermany.toString());

    }

}


